package es.uned.lsi.eped.Ejercicios.Junio2011_1A.Ejercicio2;

import es.uned.lsi.eped.DataStructures.IteratorIF;
import es.uned.lsi.eped.DataStructures.List;
import es.uned.lsi.eped.DataStructures.ListIF;

/*
 * 2. Dise�e un m�todo ListIF<int> repeated(ListIF<int> input) que genere una
lista (de salida) con el numero de veces que aparece cada elemento en la lista de entrada,
donde el elemento i-�simo de la lista de salida se corresponde con el numero de veces que
aparece el elemento i-�simo (distinto) en la de entrada (por ejemplo, para la entrada
[0; 3; 0; 2; 8; 8; 4; 5] el resultado ser�a [2; 1; 1; 2; 1; 1] (0 aparece 2 veces, 3 aparece 1,
2 est� una vez, hay 2 ochos, un cuatro y un cinco). Se valorar� la eficiencia.
 *
 */

public class Ejercicio_2 {

	public static ListIF<Integer> repeated(ListIF<Integer> input) {
		ListIF<Integer> listaFrecuencias = new List<Integer>();
		ListIF<Integer> listaVisitados = new List<Integer>();

		Integer elementoBuscado = 0;
		Integer frecuencia = 1;

		int inputLength = input.size();
		for (int i = 1; i <= inputLength; i++) {
			frecuencia = 0;

			elementoBuscado = input.get(i);

			if (!listaVisitados.contains(elementoBuscado)) {
				for (int j = i; j <= inputLength; j++) {
					if (elementoBuscado.equals(input.get(j))) {
						frecuencia++;
					}
				}
				listaVisitados.insert(elementoBuscado, 1);
				listaFrecuencias.insert(frecuencia, listaFrecuencias.size() + 1);
			}
		}

		return listaFrecuencias;
	}

	public static void muestraLista(ListIF<Integer> lista) {
		IteratorIF<Integer> it = lista.iterator();
		String salida = "Lista: [ ";

		while (it.hasNext()) {
			salida += it.getNext() + ", ";
		}
		salida = salida.substring(0, salida.length() - 2);
		System.out.println(salida + " ]");
	}

	public static void main(String[] args) {

		ListIF<Integer> listaOrigen = new List<Integer>();
		listaOrigen.insert(5, 1);
		listaOrigen.insert(4, 1);
		listaOrigen.insert(8, 1);
		listaOrigen.insert(8, 1);
		listaOrigen.insert(2, 1);
		listaOrigen.insert(0, 1);
		listaOrigen.insert(3, 1);
		listaOrigen.insert(0, 1);

		muestraLista(listaOrigen);

		ListIF<Integer> listaFrecuencias = repeated(listaOrigen);

		System.out.println("Resultado");
		muestraLista(listaFrecuencias);
	}
}
