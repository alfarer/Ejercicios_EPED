package es.uned.lsi.eped.Ejercicios.Junio2011_1A.Ejercicio1;

public class Ejercicio_1 {

	/*
	 * Dado un n�mero obtiene la lista de d�gitos que lo representa *
	 *
	 * @param numero N�mero que se divide en d�gitos *
	 *
	 * @Pre: 0 <= numero
	 */
	public static ListIF<Integer> digits(int numero) {
		ListIF<Integer> listaDigitos = new List<Integer>();
		Integer auxiliarElement = 0;
		numero = numero * 10;
		int index = 1;
		Integer previousDigit = 0;
		Integer initialValue = 0;

		while (numero > 9) {
			numero = numero / 10;
			listaDigitos.insert(numero, 1);
		}

		IteratorIF<Integer> listaIterator = listaDigitos.iterator();

		while (listaIterator.hasNext()) {
			auxiliarElement = listaIterator.getNext();
			initialValue = auxiliarElement;
			if (index != 1) {
				auxiliarElement = auxiliarElement - (previousDigit * 10);
				listaDigitos.set(index, auxiliarElement);
			}

			index++;
			previousDigit = initialValue;
		}

		return listaDigitos;
	}

	public static void main(String[] args) {
		ListIF<Integer> listaDigitos = digits(543210);

		IteratorIF<Integer> listaIterator = listaDigitos.iterator();

		System.out.print("Digitos [ ");

		while (listaIterator.hasNext()) {
			System.out.print(listaIterator.getNext() + ", ");

		}
		System.out.println("]");
	}
}
