package es.uned.lsi.eped.Ejercicios.Jun2015S2.Ejercicio2;

/*
 * Representa un par para la compresi�on RLE. Se hace p�ublica por conveniencia
 * para resolver la implementaci�on de la clase iteradora
 */
public class RLEPair<T> {

	private T element;
	private int freq;

	RLEPair(T e, int f) {
		element = e;
		freq = f;
	}

	public T getElement() {
		return element;
	}

	public int getFreq() {
		return freq;
	}

	public void setElement(T e) {
		element = e;
	}

	public void setFreq(int f) {
		freq = f;
	}
}
