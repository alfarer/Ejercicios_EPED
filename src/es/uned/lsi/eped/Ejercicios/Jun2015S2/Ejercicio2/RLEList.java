package es.uned.lsi.eped.Ejercicios.Jun2015S2.Ejercicio2;

/* Representa una lista comprimida mediante RLE (run-length */
/* encoding) */
public class RLEList<T> {
//	private final int size;
//	private final T mode;
//	private final ListIF<RLEPair<T>> data;
//
//	/* The empty constructor is included to make it easy setting up */
//	/* the initial values when actually constructing a compressed */
//	/* RLE list */
//	public RLEList() {
//		size = 0;
//		mode = null;
//		data = null;
//	}
//
//	/* Creates a Run-Length Encoded compressed list out of a list */
//	/* possibly including element repetitions and keeping the order */
//	/* of occurrence of the original elements */
//	/* @param the list to be compressed */
//	public RLEList(ListIF<T> input) {
//		this();
//		size = input.getLength();
//		data = compress(input, null, 0);
//		mode = computeMode();
//	}
//
//	/* Private method meant to carry out the actual compression */
//	/* @param the input (list) to be compressed */
//	private RLEList<T> compress(ListIF<T> input, T elem, int count) {
//// caso base: entrada vac´ıa
//		if (input.isEmpty()) {
//			final ListDynamic<RLEPair<T>> init = new ListDynamic<RLEPair<T>>();
//			if (elem == null)
//				return init;
//			final RLEPair<T> aPair = new RLEPair(elem, count);
//			return init.insert(aPair);
//		}
//// Empieza el conteo de un nuevo grupo de elementos repetidos
//		if (elem == null)
//			return compress(input.getTail(), input.getFirst(), 1);
//// Contin´ua el conteo de un grupo de elementos repetidos
//		if (elem.equals(input.getFirst()))
//			return compress(input.getTail(), elem, count++);
//// Termina el conteo de un grupo de elementos repetidos
//		final RLEPair<T> aPair = new RLEPair(elem, count);
//		return compress(input.getTail(), null, 0).insert(aPair);
//	}

}
