package es.uned.lsi.eped.Ejercicios.Jun2015S2.Ejercicio2;

public interface RLEListIF<E> {

	/* [0’5 puntos] devuelve el n´umero total de elementos de la lista */
	/* En el ejemplo deber´ıa devolver 12, no 4 */
	public int size ();
	/* [1 punto] devuelve la lista descomprimida */
	public ListIF<E> decompress();
	/* [1 punto] calcula la moda (el elemento m´as repetido de la *
	* lista). En el ejemplo, ser´ıa el 1, ya que se repite 6 veces */
	public E mode ();
	
}
