package es.uned.lsi.eped.Ejercicios.Septiembre2011Original.Ejercicio_x;

public class List<E> extends Sequence<E> implements ListIF<E> {

	/* Constructor por defecto: crea una lista vacía */
	public List() {
		super();
	}

	/*
	 * Constructor por copia: delega en el constructor por copia * de la secuencia
	 */
	public List(List<E> s) {
		super(s);
	}

	/* Devuelve el elemento pos-ésimo de la lista */
	@Override
	public E get(int pos) {
		NodeSequence node = getNode(pos);
		return node.getValue();
	}

	/* Modifica el elemento pos-ésimo de la lista */
	@Override
	public void set(int pos, E e) {
		NodeSequence node = getNode(pos);
		node.setValue(e);
	}

	/*
	 * Inserta un nuevo elemento en la lista en la posición * indicada
	 */
	@Override
	public void insert(E elem, int pos) {
		NodeSequence newNode = new NodeSequence(elem);
		if (pos == 1) {
			newNode.setNext(this.firstNode);
			this.firstNode = newNode;
		} else {
			NodeSequence previousNode = getNode(pos - 1);
			NodeSequence nextNode = previousNode.getNext();
			previousNode.setNext(newNode);
			newNode.setNext(nextNode);
		}
		this.size++;
	}

	/* Elimina el elemento pos-ésimo de la lista */
	@Override
	public void remove(int pos) {
		if (pos == 1) {
			this.firstNode = this.firstNode.getNext();
		} else {
			NodeSequence previousNode = getNode(pos - 1);
			NodeSequence nextNode = previousNode.getNext().getNext();
			previousNode.setNext(nextNode);
		}
		this.size--;
	}

	/* Obtiene el elemento dado por index, el primer elemento es el cero */
	@Override
	public E getForIndex(int index) {
		NodeSequence node = getNode(index + 1);
		return node.getValue();
	}

	public static void main(String[] args) {
		List lista = new List();

		lista.insert(11, 1);
		lista.insert(22, 2);
		lista.insert(33, 3);
		lista.insert(44, 4);

		System.out.println("Posici�n 0: " + lista.getForIndex(0));
	}
}
