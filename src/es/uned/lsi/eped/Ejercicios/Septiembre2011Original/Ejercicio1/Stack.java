package es.uned.lsi.eped.Ejercicios.Septiembre2011Original.Ejercicio1;

public class Stack<E> extends Sequence<E> implements StackIF<E> {

	/* Constructor por defecto: crea una pila vacía */
	public Stack() {
		super();
	}

	/*
	 * Constructor por copia: delega en el constructor por copia * de la secuencia
	 */
	public Stack(Stack<E> s) {
		super(s);
	}

	/* Devuelve el elemento en la cima de la pila */
	@Override
	public E getTop() {
		return this.firstNode.getValue();
	}

	/* Añade un nuevo elemento a la cima de la pila */
	@Override
	public void push(E elem) {
		NodeSequence newNode = new NodeSequence(elem);
		if (!isEmpty()) {
			newNode.setNext(this.firstNode);
		}
		this.firstNode = newNode;
		this.size++;
	}

	/* Elimina el elemento situado en la cima de la pila */
	@Override
	public void pop() {
		this.firstNode = this.firstNode.getNext();
		this.size--;
	}

	/* Concatena la pila par�metro a la pila llamante */
	public StackIF<E> concatNoRecursivo(StackIF<E> stk) {
		StackIF<E> pilaAuxiliar = new Stack<E>();

		while (!stk.isEmpty()) {
			pilaAuxiliar.push(stk.getTop());
			stk.pop();
		}

		while (!pilaAuxiliar.isEmpty()) {
			this.push(pilaAuxiliar.getTop());
			pilaAuxiliar.pop();
		}
		return this;
	}

	/* Concatena la pila par�metro a la pila llamante */
	@Override
	public StackIF<E> concat(StackIF<E> stk) {
		E elemento;

		if (stk.isEmpty()) {
			return this;
		} else {
			elemento = stk.getTop();
			stk.pop();
			concat(stk);
			this.push(elemento);
			return this;
		}
	}

	public static void main(String[] args) {
		StackIF<Integer> pilaInicial = new Stack<>();
		StackIF<Integer> pilaAdicional = new Stack<>();
		StackIF<Integer> pilaConcatenada = new Stack<>();

		pilaInicial.push(44);
		pilaInicial.push(33);
		pilaInicial.push(22);
		pilaInicial.push(11);

		muestraPila(pilaInicial);

		pilaAdicional.push(3);
		pilaAdicional.push(2);
		pilaAdicional.push(1);

		muestraPila(pilaAdicional);

		pilaConcatenada = pilaInicial.concat(pilaAdicional);

		muestraPila(pilaConcatenada);
	}

	public static void muestraPila(StackIF<Integer> pila) {
		QueueIF<Integer> filaAuxiliar = new Queue<>();

		System.out.print("Pila: [ ");

		IteratorIF<Integer> itPila = pila.iterator();

		while (itPila.hasNext()) {
			System.out.print(itPila.getNext() + ", ");
		}
		System.out.println("]");
	}
}
