package es.uned.lsi.eped.Ejercicios.Septiembre2011Original.Ejercicio2;

public class Main_para_probar {

	public static void main(String[] args) {
		QueueIF<Integer> colaLlamante = new Queue<Integer>();

		colaLlamante.enqueue(1);
		colaLlamante.enqueue(2);
		colaLlamante.enqueue(3);
		colaLlamante.enqueue(4);
		colaLlamante.enqueue(5);

		muestraCola(colaLlamante);

		QueueIF<Integer> colaParametro = new Queue<Integer>();

		colaParametro.enqueue(2);
		colaParametro.enqueue(3);
		colaParametro.enqueue(5);
		colaParametro.enqueue(1);
		colaParametro.enqueue(2);
		colaParametro.enqueue(3);
		colaParametro.enqueue(4);
		colaParametro.enqueue(5);
		colaParametro.enqueue(7);
		colaParametro.enqueue(8);
		colaParametro.enqueue(9);

		muestraCola(colaParametro);

		System.out.println("Es subcola: " + colaLlamante.isSubqueue(colaParametro));
	}

	public static void muestraCola(QueueIF<Integer> cola) {
		IteratorIF<Integer> iteratorCola = cola.iterator();

		System.out.print("Cola: [ ");
		while (iteratorCola.hasNext()) {
			System.out.print(iteratorCola.getNext() + ", ");
		}
		System.out.println(" ]");
	}

}
