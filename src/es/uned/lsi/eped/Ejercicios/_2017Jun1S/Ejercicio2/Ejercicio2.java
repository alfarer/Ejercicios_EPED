package es.uned.lsi.eped.Ejercicios._2017Jun1S.Ejercicio2;

public class Ejercicio2 {

	/**
	 * Decida si el �rbol dado por par�metro es k-ario
	 *
	 * @param tree arbol
	 * @param k    grado de maxima amplitude del arbol y subarboles
	 * @return true si es k-ario, false en caso contrario
	 */

	static boolean isKAryTree(GTreeIF<Integer> tree, int k) {
		if (tree.isEmpty()) {
			return true;
		}
		final ListIF<GTreeIF<Integer>> children = tree.getChildren();
		final IteratorIF<GTreeIF<Integer>> it = children.iterator();
		boolean allKAry = true;
		if (children.size() > k) {
			return false;
		}
		while (it.hasNext() && allKAry) {
			allKAry = isKAryTree(it.getNext(), k);
		}
		return allKAry;
	}

	public static void main(String[] args) {
		final GTreeIF<Integer> gTree = new GTree<Integer>();
		gTree.setRoot(1);
		final GTreeIF<Integer> gTree11 = new GTree<Integer>();
		gTree11.setRoot(11);
		final GTreeIF<Integer> gTree12 = new GTree<Integer>();
		gTree12.setRoot(12);
		final GTreeIF<Integer> gTree13 = new GTree<Integer>();
		gTree13.setRoot(13);

		gTree.addChild(1, gTree13);
		gTree.addChild(1, gTree12);
		gTree.addChild(1, gTree11);

		final GTreeIF<Integer> gTree31 = new GTree<Integer>();
		gTree31.setRoot(31);
		final GTreeIF<Integer> gTree32 = new GTree<Integer>();
		gTree32.setRoot(32);
		final GTreeIF<Integer> gTree33 = new GTree<Integer>();
		gTree33.setRoot(33);
		final GTreeIF<Integer> gTree34 = new GTree<Integer>();
		gTree34.setRoot(34);

		gTree13.addChild(1, gTree34);
		gTree13.addChild(1, gTree33);
		gTree13.addChild(1, gTree32);
		gTree13.addChild(1, gTree31);

		System.out.println("El �rbol es k-ario 3: " + isKAryTree(gTree, 3));
	}

}
