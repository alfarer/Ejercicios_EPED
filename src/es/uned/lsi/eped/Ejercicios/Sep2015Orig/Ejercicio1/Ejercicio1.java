package es.uned.lsi.eped.Ejercicios.Sep2015Orig.Ejercicio1;

public class Ejercicio1 {

	/**
	 * Se desea un metodo tal que, tomando como precondicion que todos los numeros
	 * de la lista sean no negativos, devuelva el mayor prefijo cuya suma no supere
	 * n
	 *
	 * @param l lista con enteros
	 * @param n numero maximo del sumatorio de prefijos
	 * @return prefijos lista con los prefijos
	 */
	static ListIF<Integer> maxPrefix(ListIF<Integer> l, int n) {
		ListIF<Integer> prefijos = new List<Integer>();
		Integer primerNumero = 0;

		if (!l.isEmpty()) {
			primerNumero = l.get(1);
			if (n - primerNumero >= 0) {
				n = n - primerNumero;
				l.remove(1);
				prefijos = maxPrefix(l, n);
				prefijos.insert(primerNumero, 1);
				return prefijos;
			} else {
				return prefijos;
			}
		} else {
			return prefijos;
		}
	}

	public static void main(String[] args) {
		final ListIF<Integer> lista = new List<Integer>();

		lista.insert(1, 1);
		lista.insert(2, 1);
		lista.insert(3, 1);
		lista.insert(0, 1);
		lista.insert(5, 1);
		lista.insert(4, 1);

		mostrarLista(lista, "Lista");

		final ListIF<Integer> prefijos = maxPrefix(lista, 10);
		mostrarLista(prefijos, "Prefijos");

	}

	public static void mostrarLista(ListIF lista, String nombreLista) {
		final IteratorIF it = lista.iterator();
		System.out.print(nombreLista + ": [ ");
		while (it.hasNext()) {
			System.out.print(it.getNext() + ", ");
		}
		System.out.println("]");
	}

}
