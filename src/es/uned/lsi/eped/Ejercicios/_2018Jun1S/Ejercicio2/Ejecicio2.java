package es.uned.lsi.eped.Ejercicios._2018Jun1S.Ejercicio2;

public class Ejecicio2 {

	/**
	 * Combina las pila s1 y s2 que estan ordenadas de menor a mayor (es decir, el
	 * numero mas peque�o esta en la cima de cada pila) en una nueva pila de enteros
	 * que contenga todos los elementos presentes en s1 y s2 y que est� ordenada de
	 * menor a mayor.
	 *
	 * @param s1 pila ordenada de mayor a menor en la cima
	 * @param s2 pila ordenada de mayor a menor en la cima
	 * @return sMix pila mix de s1 y s2
	 */
	static public StackIF<Integer> mixStacks(StackIF<Integer> s1, StackIF<Integer> s2) {
		final StackIF<Integer> sMix = new Stack<Integer>();
		Integer s1Integer = null;
		Integer s2Integer = null;

		final StackIF<Integer> s1Reverse = new Stack<Integer>();
		while (!s1.isEmpty()) {
			s1Reverse.push(s1.getTop());
			s1.pop();
		}

		final StackIF<Integer> s2Reverse = new Stack<Integer>();
		while (!s2.isEmpty()) {
			s2Reverse.push(s2.getTop());
			s2.pop();
		}

		while (!s1Reverse.isEmpty() && !s2Reverse.isEmpty()) {
			s1Integer = s1Reverse.getTop();
			s2Integer = s2Reverse.getTop();
			if (s1Integer >= s2Integer) {
				sMix.push(s1Integer);
				s1.push(s1Integer);
				s1Reverse.pop();
			} else {
				sMix.push(s2Integer);
				s2.push(s2Integer);
				s2Reverse.pop();
			}
		}

		while (!s1Reverse.isEmpty()) {
			s1Integer = s1Reverse.getTop();
			sMix.push(s1Integer);
			s1.push(s1Integer);
			s1Reverse.pop();
		}

		while (!s2Reverse.isEmpty()) {
			s2Integer = s2Reverse.getTop();
			sMix.push(s2Integer);
			s2.push(s2Integer);
			s2Reverse.pop();
		}

		return sMix;
	}

	public static void main(String[] args) {
		final StackIF<Integer> s1 = new Stack<Integer>();
		s1.push(5);
		s1.push(3);
		s1.push(1);

		final StackIF<Integer> s2 = new Stack<Integer>();
		s2.push(10);
		s2.push(8);
		s2.push(6);
		s2.push(4);
//		s2.push(2);

		final StackIF<Integer> sMix = mixStacks(s1, s2);

		showStack(sMix, "sMix");
		showStack(s1, "s1");
		showStack(s2, "s2");
	}

	static public void showStack(StackIF<Integer> stack, String name) {
		final IteratorIF it = stack.iterator();

		System.out.print("Pila " + name + ": [ ");
		while (it.hasNext()) {
			System.out.print(it.getNext() + "<= ");
		}
		System.out.println(" ]");
	}
}
