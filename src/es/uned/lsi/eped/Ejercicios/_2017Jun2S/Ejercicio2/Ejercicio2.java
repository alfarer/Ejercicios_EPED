package es.uned.lsi.eped.Ejercicios._2017Jun2S.Ejercicio2;

public class Ejercicio2 {

	/**
	 * obtenga el m�nimo valor de k para el que se cumple que el �rbol dado por
	 * par�metro es k-ario
	 *
	 * @param tree
	 * @return minima k-aridad
	 */
	static int getMinArity(GTreeIF<Integer> tree) {
		int k = 0;
		int aux = 0;
		if (tree.isEmpty()) {
			return k;
		} else {
			final ListIF<GTreeIF<Integer>> children = tree.getChildren();
			final IteratorIF<GTreeIF<Integer>> it = children.iterator();
			k = children.size();

			while (it.hasNext()) {
				aux = getMinArity(it.getNext());
				if (k < aux) {
					k = aux;
				}
			}
			return k;
		}
	}

	public static void main(String[] args) {

		final GTreeIF<Integer> gTree = new GTree<Integer>();
		gTree.setRoot(1);
		final GTreeIF<Integer> gTree11 = new GTree<Integer>();
		gTree11.setRoot(11);
		final GTreeIF<Integer> gTree12 = new GTree<Integer>();
		gTree12.setRoot(12);
		final GTreeIF<Integer> gTree13 = new GTree<Integer>();
		gTree13.setRoot(13);

		gTree.addChild(1, gTree13);
		gTree.addChild(1, gTree12);
		gTree.addChild(1, gTree11);

		final GTreeIF<Integer> gTree31 = new GTree<Integer>();
		gTree31.setRoot(31);
		final GTreeIF<Integer> gTree32 = new GTree<Integer>();
		gTree32.setRoot(32);
		final GTreeIF<Integer> gTree33 = new GTree<Integer>();
		gTree33.setRoot(33);
		final GTreeIF<Integer> gTree34 = new GTree<Integer>();
		gTree34.setRoot(34);

		gTree13.addChild(1, gTree34);
		gTree13.addChild(1, gTree33);
		gTree13.addChild(1, gTree32);
		gTree13.addChild(1, gTree31);

		System.out.println("Minima k-aridad del arbol: " + getMinArity(gTree));

	}

}
