package es.uned.lsi.eped.Ejercicios._2018Jun2S.Ejercicio2;

public class Ejercicio2 {

	static public void removeNonMultiplesQ(int k, QueueIF<Integer> q) {
		final int length = q.size();
		Integer element = null;

		for (int i = 1; i <= length; i++) {
			element = q.getFirst();
			q.dequeue();
			if (element % k == 0) {
				q.enqueue(element);
			}
		}
	}

	static public void removeNonMultiplesS(int k, StackIF<Integer> s) {
		final StackIF<Integer> aux = new Stack<Integer>();
		Integer element = null;
		while (!s.isEmpty()) {
			element = s.getTop();
			s.pop();
			if (element % k == 0) {
				aux.push(element);
			}
		}

		while (!aux.isEmpty()) {
			s.push(aux.getTop());
			aux.pop();
		}
	}

	public static void main(String[] args) {
		final QueueIF<Integer> q = new Queue<Integer>();

		q.enqueue(1);
		q.enqueue(2);
		q.enqueue(3);
		q.enqueue(4);
		q.enqueue(5);
		q.enqueue(6);
		q.enqueue(7);
		q.enqueue(8);
		q.enqueue(9);
		q.enqueue(10);

		showQueue(q, "Cola inicial");

		removeNonMultiplesQ(3, q);

		showQueue(q, "Cola m�ltiplos de 3");

		final StackIF<Integer> s = new Stack<Integer>();

		s.push(10);
		s.push(9);
		s.push(8);
		s.push(7);
		s.push(6);
		s.push(5);
		s.push(4);
		s.push(3);
		s.push(2);
		s.push(1);

		showStack(s, "Pila inicial");

		removeNonMultiplesS(3, s);

		showStack(s, "Pila m�ltiplos de 3");
	}

	static public void showStack(StackIF<Integer> stack, String name) {
		final IteratorIF<Integer> it = stack.iterator();

		System.out.print("Pila " + name + ": [ ");
		while (it.hasNext()) {
			System.out.print(it.getNext() + "<= ");
		}
		System.out.println(" ]");
	}

	static public void showQueue(QueueIF<Integer> queue, String name) {
		final IteratorIF<Integer> it = queue.iterator();

		System.out.print("Fila " + name + ": [ ");
		while (it.hasNext()) {
			System.out.print(it.getNext() + ", ");
		}
		System.out.println(" ]");
	}
}
