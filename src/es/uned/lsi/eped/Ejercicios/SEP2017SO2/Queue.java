package es.uned.lsi.eped.Ejercicios.SEP2017SO2;

public class Queue<E> extends Sequence<E> implements QueueIF<E> {

	private NodeSequence lastNode;

	/* Constructor por defecto: crea una cola vacía */
	public Queue() {
		super(); /* Construimos la secuencia vacía */
		this.lastNode = null; /* No hay último nodo */
	}

	/*
	 * Constructor por copia: delega en el constructor por copia * de la secuencia
	 */
	public Queue(Queue<E> s) {
		super(s); /* Copiamos la secuencia de la cola original */
		/* Recorremos la secuencia de la cola copia para encontrar su último nodo */
		if (this.isEmpty()) {
			this.lastNode = null;
		} else {
			NodeSequence node = this.getNode(this.size);
			this.lastNode = node;
		}
	}

	/* Devuelve el primer elemento de la cola */
	@Override
	public E getFirst() {
		return this.firstNode.getValue();
	}

	/* Añade un nuevo elemento al final de la cola */
	@Override
	public void enqueue(E elem) {
		NodeSequence newNode = new NodeSequence(elem);
		if (isEmpty()) {
			this.firstNode = newNode;
		} else {
			this.lastNode.setNext(newNode);
		}
		this.lastNode = newNode;
		this.size++;
	}

	/* Elimina el primer elemento de la cola */
	@Override
	public void dequeue() {
		this.firstNode = this.firstNode.getNext();
		this.size--;
		if (this.size == 0) {
			this.lastNode = null;
		}
	}

	/* Vacía la cola */
	@Override
	public void clear() {
		super.clear(); /* Vaciamos la secuencia */
		this.lastNode = null; /* No hay último nodo */
	}

	/* Convierte la cola a pal�ndromo de s� misma */
	@Override
	public void palindromeQueue() {
		QueueIF<E> auxiliarQueue = new Queue<E>(this);
		StackIF<E> auxiliarStack = new Stack<E>();
		E intermediateElement = null;

		int forLength = auxiliarQueue.size();
		for (int i = 0; i < forLength; i++) {
			intermediateElement = auxiliarQueue.getFirst();
			auxiliarStack.push(intermediateElement);
			auxiliarQueue.dequeue();
		}

		forLength = auxiliarStack.size();
		for (int i = 0; i < forLength; i++) {
			intermediateElement = auxiliarStack.getTop();
			this.enqueue(intermediateElement);
			auxiliarStack.pop();
		}

	}

}
