package es.uned.lsi.eped.Ejercicios.SEP2017SO2;

/**
 * Ejercicio programe en java un m�todo: void palindromeQueue() dentro del TAD
 * QueueIF<T>, de tal manera que los n primeros coincidan con los de la cola
 * origina (en el mismo orden) y que, adem�s, sea capic�a. Por ejemplo: q = [1,
 * 3, 4, 25, 2, 6, 3, 1, 6] q.palindromeQueue(); q = [1, 3, 4, 25, 2, 6, 3, 1,
 * 6, 6, 1, 3, 6, 2, 25, 4, 3, 1]
 *
 * @author alfarer
 *
 */

public class SEP2017SO2_Main_para_probar {

	public static void main(String[] args) {

		QueueIF<Integer> queueToCapicua = new Queue<Integer>();

		queueToCapicua.enqueue(1);
		queueToCapicua.enqueue(2);
		queueToCapicua.enqueue(3);

		queueToCapicua.palindromeQueue();

		int forLength = queueToCapicua.size();
		System.out.print("q = [ ");
		for (int i = 0; i < forLength; i++) {
			System.out.print(queueToCapicua.getFirst() + ", ");
			queueToCapicua.dequeue();
		}
		System.out.println("]");
	}
}
