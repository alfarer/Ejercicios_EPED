/**
 * Ejercicio programe en java un m�todo: void palindromeQueue()
 * dentro del TAD QueueIF<T>, de tal manera que los n primeros coincidan con los de la cola 
 * origina (en el mismo orden) y que, adem�s, sea capic�a. Por ejemplo:
 *
 * q = [1, 3, 4, 25, 2, 6, 3, 1, 6];
 * q.palindromeQueue();
 * q = [1, 3, 4, 25, 2, 6, 3, 1, 6, 6, 1, 3, 6, 2, 25, 4, 3, 1];
 * 
 * @author alfarer
 *
 */